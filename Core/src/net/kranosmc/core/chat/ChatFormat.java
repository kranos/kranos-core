package net.kranosmc.core.chat;

import java.util.UUID;

import net.kranosmc.core.api.account.Rank;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.kranosmc.core.api.account.Account;

public class ChatFormat implements Listener {

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		UUID uuid = player.getUniqueId();
		Rank rank = Account.getRank(uuid);

		event.setFormat(rank.getPrefix() + " " + rank.getColor() + player.getName() + " §8» " + rank.getColor()
		+ event.getMessage());
	}

}
