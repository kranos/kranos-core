package net.kranosmc.core.listeners;

import java.util.UUID;

import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.database.MySQL;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		UUID uuid = player.getUniqueId();

		player.setAllowFlight(false);

		if (MySQL.containsPlayer(uuid)) {
			Account.loadAccount(uuid);
		} else if (!(MySQL.containsPlayer(uuid))) {
			Account.createAccount(uuid);
		} else {
			player.kickPlayer("§c§lError loading/creating your account. Please login again.");
		}
	}

}
