package net.kranosmc.core.listeners;

import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

/**
 * Created by Phineas (phineas.pw) on 08/07/2016
 * For Kranos (kranosmc.net)
 */
public class ShiftListener implements Listener {
    //:3
    @EventHandler
    public void sneakSounds(PlayerToggleSneakEvent event) {
        if (event.getPlayer().getName().equals("Palombo")) {
            if (!event.getPlayer().isSneaking()) {
                event.getPlayer().getWorld().playSound(event.getPlayer().getLocation(), Sound.WOLF_BARK, 1f, 1f);
            }
        }
    }
}