package net.kranosmc.core.commands;

import java.util.UUID;

import net.kranosmc.core.api.account.Rank;
import net.kranosmc.core.database.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.api.messages.M;

public class SetRankCMD implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("setrank")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					if (args.length < 2) {
						showHelp(sender);
					} else {
						OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
						UUID targetUUID = target.getUniqueId();

						if (MySQL.containsPlayer(targetUUID)) {
							if (Rank.valueOf(args[1].toUpperCase()) == Rank.OWNER ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.HEADDEV ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.ADMIN ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.DEV ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.JRDEV ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.MODPLUS ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.MOD ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.HELPERPLUS ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.HELPER ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.BUILDER ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.YOUTUBER ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.GOD ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.MVP ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.VIP ||
									Rank.valueOf(args[1].toUpperCase()) == Rank.ALL){
								sender.sendMessage(M.playerRankSet(targetUUID, Rank.valueOf(args[1].toUpperCase())));
								Account.setRank(targetUUID, Rank.valueOf(args[1].toUpperCase()));
							} else {
								sender.sendMessage(M.rankDoesNotExist());
							}
						} else {
							sender.sendMessage(M.playerDoesNotExist());
						}
					}
				} else if (!Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					sender.sendMessage(M.neededRank(Rank.ADMIN));
				}
			}
		}
		return true;
	}

	public void showHelp(CommandSender sender) {
		sender.sendMessage(M.rankPrefix + "/setrank <player> [rank]");
	}
}