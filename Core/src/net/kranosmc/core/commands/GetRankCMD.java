package net.kranosmc.core.commands;

import java.util.UUID;

import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.api.account.Rank;
import net.kranosmc.core.database.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kranosmc.core.api.messages.M;

public class GetRankCMD implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("getrank")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.MOD)) {
					if (args.length < 1) {
						showHelp(sender);
					} else {
						OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
						UUID targetUUID = target.getUniqueId();

						if (MySQL.containsPlayer(targetUUID)) {
							sender.sendMessage(M.playerRank(targetUUID));
						} else {
							sender.sendMessage(M.playerDoesNotExist());
						}
					}
				} else if (!Account.canDo(player.getUniqueId(), Rank.MOD)) {
					sender.sendMessage(M.neededRank(Rank.MOD));
				}
			}
		}
		return true;
	}

	public void showHelp(CommandSender sender) {
		sender.sendMessage(M.rankPrefix + "/getrank <player>");
	}
}