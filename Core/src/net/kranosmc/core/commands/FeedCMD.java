package net.kranosmc.core.commands;

import net.kranosmc.core.api.account.Rank;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.api.messages.M;

public class FeedCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("feed")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					player.setFoodLevel(20);
					sender.sendMessage(M.playerFeed());
				} else if (!Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					sender.sendMessage(M.neededRank(Rank.ADMIN));
				}
			}
		}
		return true;
	}
}