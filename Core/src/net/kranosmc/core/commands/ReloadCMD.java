package net.kranosmc.core.commands;

import net.kranosmc.core.Core;
import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.api.account.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import net.kranosmc.core.api.messages.M;

public class ReloadCMD implements CommandExecutor, Listener {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("reload")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					Bukkit.getServer().reload();
					sender.sendMessage(M.serverReloaded());
				} else if (!Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					sender.sendMessage(M.neededRank(Rank.ADMIN));
				}
			}
		}
		return true;
	}

	@EventHandler
	public void reloadCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();
		String[] command = message.split(" ");
		String[] args = new String[command.length - 1];

		System.arraycopy(command, 1, args, 0, args.length);
		if (command[0].equalsIgnoreCase("/reload")) {
			event.setCancelled(true);
			onCommand(player, Core.getInstance().getCommand("reload"), "reload", args);
		}
	}
}