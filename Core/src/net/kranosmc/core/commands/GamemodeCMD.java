package net.kranosmc.core.commands;

import net.kranosmc.core.api.account.Rank;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.api.messages.M;

public class GamemodeCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("gamemode")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					if (args.length < 1) {
						showHelp(sender);
					} else {
						if (args[0].contains("cr") || args[0].equalsIgnoreCase("c")) {
							player.setGameMode(GameMode.CREATIVE);
							sender.sendMessage(M.gamemodeChangedToCreative());
						} else if (args[0].contains("su") || args[0].equalsIgnoreCase("s")) {
							player.setGameMode(GameMode.SURVIVAL);
							sender.sendMessage(M.gamemodeChangedToSurvival());
						} else if (args[0].contains("ad") || args[0].equalsIgnoreCase("a")) {
							player.setGameMode(GameMode.ADVENTURE);
							sender.sendMessage(M.gamemodeChangedToAdventure());
						} else {
							sender.sendMessage(M.gamemodeDoesNotExist());
						}
					}
				} else if (!Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					sender.sendMessage(M.neededRank(Rank.ADMIN));
				}
			}
		}
		return true;
	}

	public void showHelp(CommandSender sender) {
		sender.sendMessage(M.utilPrefix + "/gamemode [creative/survival/adventure]");
	}
}