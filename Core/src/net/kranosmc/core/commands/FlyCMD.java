package net.kranosmc.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.api.account.Rank;
import net.kranosmc.core.api.messages.M;

public class FlyCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("fly")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.MODPLUS)) {
					if (player.getAllowFlight()) {
						player.setAllowFlight(false);
						sender.sendMessage(M.playerFlyDisabled());
					} else if (!player.getAllowFlight()) {
						player.setAllowFlight(true);
						sender.sendMessage(M.playerFlyEnabled());
					}
				} else if (!Account.canDo(player.getUniqueId(), Rank.MODPLUS)) {
					sender.sendMessage(M.neededRank(Rank.MODPLUS));
				}
			}
		}
		return true;
	}
}