package net.kranosmc.core.commands;

import net.kranosmc.core.api.account.Account;
import net.kranosmc.core.api.account.Rank;
import net.kranosmc.core.api.messages.M;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HealCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("heal")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					player.setHealth(20D);
					sender.sendMessage(M.playerHeal());
				} else if (!Account.canDo(player.getUniqueId(), Rank.ADMIN)) {
					sender.sendMessage(M.neededRank(Rank.ADMIN));
				}
			}
		}
		return true;
	}
}