package net.kranosmc.core;

import net.kranosmc.core.database.SQLConnection;
import net.kranosmc.core.commands.FlyCMD;
import net.kranosmc.core.listeners.ShiftListener;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import net.kranosmc.core.commands.GetRankCMD;
import net.kranosmc.core.commands.SetRankCMD;
import net.kranosmc.core.listeners.JoinListener;
import net.kranosmc.core.chat.ChatFormat;
import net.kranosmc.core.commands.FeedCMD;
import net.kranosmc.core.commands.GamemodeCMD;
import net.kranosmc.core.commands.HealCMD;
import net.kranosmc.core.commands.ReloadCMD;
import org.bukkit.scheduler.BukkitRunnable;

public class Core extends JavaPlugin {

	private static Core instance = null;
	public static Core getInstance() { return instance; }

	private static SQLConnection sqlConnection = null;
	public static SQLConnection getSQLConnection() {return sqlConnection; }

	public void onEnable(){
		instance = this;

		sqlConnection = new SQLConnection(instance, "142.4.217.117", "3306", "mc_1778", "mc_1778", "7304ad667b");
		new BukkitRunnable() {
			public void run() {
				/**
				 *
				 * Connects to Kranos MySQL database asynchronously
				 *
				 */
				sqlConnection.openConnection();
			}
		}.runTaskAsynchronously(instance);

		register();
	}

	public void onDisable(){
		sqlConnection.closeConnection();
	}

	public void registerListener(Listener... listener) {
		for (Listener l : listener) {
			this.getServer().getPluginManager().registerEvents(l, this);
		}
	}

	public void registerCommand(String command, CommandExecutor commandExecutor){
		this.getCommand(command).setExecutor(commandExecutor);
	}

	public void register() {
		registerListener(new JoinListener());
		registerListener(new ChatFormat());
		registerListener(new ReloadCMD());
		registerListener(new ShiftListener());
		
		registerCommand("setrank", new SetRankCMD());
		registerCommand("getrank", new GetRankCMD());
		registerCommand("fly", new FlyCMD());
		registerCommand("gamemode", new GamemodeCMD());
		registerCommand("heal", new HealCMD());
		registerCommand("feed", new FeedCMD());
		registerCommand("reload", new ReloadCMD());
	}

}
