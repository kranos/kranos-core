package net.kranosmc.core.api.messages;

import java.util.UUID;

import net.kranosmc.core.api.account.Rank;
import net.kranosmc.core.database.MySQL;
import org.bukkit.Bukkit;


public class M {

	public static String rankPrefix = "§c§lRank §7";
	public static String utilPrefix = "§c§lUtils §7";

	public static String neededRank(Rank rank) {
		return rankPrefix + "This requires rank [§4" + rank.getColor() + rank.toString() + "§7].";
	}

	public static String rankDoesNotExist() {
		return rankPrefix + "§4This rank does not exist.";
	}

	public static String playerRank(UUID uuid) {
		return rankPrefix + "§a" + Bukkit.getOfflinePlayer(uuid).getName() + " §7has the rank §8["
				+ Rank.valueOf(MySQL.getRank(uuid).toString()).getColor() + Rank.valueOf(MySQL.getRank(uuid).toString())
				+ "§8]§7.";
	}

	public static String playerRankSet(UUID uuid, Rank rank) {
		return rankPrefix + "§a" + Bukkit.getOfflinePlayer(uuid).getName() + "'s §7rank has been set to §8["
				+ rank.getColor() + rank.toString().toUpperCase() + "§8]§7.";
	}
	
	public static String playerDoesNotExist() {
		return rankPrefix + "§4This player does not exist.";
	}
	
	public static String playerFlyEnabled() {
		return  utilPrefix + "§aFlying enabled.";
	}
	
	public static String playerFlyDisabled() {
		return utilPrefix + "§4Flying disabled.";
	}
	
	public static String gamemodeDoesNotExist() {
		return utilPrefix + "§4That gamemode does not exist.";
	}
	
	public static String gamemodeChangedToCreative() {
		return utilPrefix + "Your gamemode has been updated to §6§lCREATIVE§7.";
	}
	
	public static String gamemodeChangedToSurvival() {
		return utilPrefix + "Your gamemode has been updated to §6§lSURVIVAL§7.";
	}
	
	public static String gamemodeChangedToAdventure() {
		return utilPrefix + "Your gamemode has been updated to §6§lADVENTURE§7.";
	}
	
	public static String serverReloaded() {
		return utilPrefix + "Server reloaded.";
	}
	
	public static String playerFeed() {
		return utilPrefix + "You have been fed.";
	}
	
	public static String playerHeal() {
		return utilPrefix + "You have been healed.";
	}
	
	
}
