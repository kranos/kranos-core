package net.kranosmc.core.api.account;

import java.util.UUID;

import net.kranosmc.core.database.MySQL;
import org.bukkit.Bukkit;


public class Account {

	public static void createAccount(UUID uuid) {
		MySQL.createPlayerData(uuid);
		loadAccount(uuid);
	}

	public static void loadAccount(UUID uuid) {
		MySQL.getRank(uuid);
		MySQL.getShards(uuid);
		MySQL.getOverallKills(uuid);
		MySQL.getOverallDeaths(uuid);
		MySQL.getTimePlayed(uuid);

		if (MySQL.getPlayerName(uuid) != Bukkit.getPlayer(uuid).getName()) {
			MySQL.savePlayerName(uuid, Bukkit.getPlayer(uuid).getName());
		} else {
			MySQL.getPlayerName(uuid);
		}
	}

	public static void removeAccount(UUID uuid) {

	}

	public static Rank getRank(UUID uuid) {
		return Rank.valueOf(MySQL.getRank(uuid));
	}

	public static void setRank(UUID uuid, Rank rank) {
		MySQL.saveRank(uuid, rank.toString());
	}

	public static boolean canDo(UUID uuid, Rank requiredRank) {
		if ((getRank(uuid).getLevel() + 1) > requiredRank.getLevel())
			return true;
		if (((getRank(uuid).getLevel() + 1) < requiredRank.getLevel()))
			return false;
		return false;
	}

}
