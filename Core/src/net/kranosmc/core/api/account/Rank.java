package net.kranosmc.core.api.account;

public enum Rank {

	// Staff
	OWNER("Owner", "§4", 10),
	HEADDEV("Head-Dev", "§5", 10),
	ADMIN("Admin", "§c", 10),
	DEV("Dev", "§d", 9),
	JRDEV("Jr-Dev", "§d", 7),
	MODPLUS("Mod+", "§9", 8),
	MOD("Mod", "§3", 7),
	HELPERPLUS("Helper+", "§2", 6),
	HELPER("Helper", "§a", 5),

	// Special
	BUILDER("Builder", "§e", 7),
	YOUTUBER("Youtuber", "§f", 7),

	// Player
	GOD("God", "§c", 4),
	MVP("MVP", "§d", 3),
	VIP("VIP", "§a", 2),
	ALL("Member", "§7"),
	ERROR("Error", "");

	private String name, color;
	private int level;

	Rank(String name, String color) {
		this(name, color, 1);
	}

	Rank(String name, String color, int level){
		this.name = name;
		this.color = color;
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	public String getPrefix() {
		return "§8[" + getColor() + getName() + "§8]";
	}

	public String getSpacedPrefix() {
		return this == ALL ? "" : getPrefix() + " ";
	}

	public int getLevel() {
		return level;
	}

	public boolean canDo(Rank check, Rank hasRank){
		if (hasRank.getLevel() > check.getLevel()) {
			return false;
		} else {
			return true;
		}
	}
}
