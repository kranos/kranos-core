package net.kranosmc.core.database;

import java.sql.ResultSet;
import java.util.UUID;

import net.kranosmc.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.Listener;

import net.kranosmc.core.api.account.Rank;

public class MySQL implements Listener {

    public synchronized static boolean containsPlayer(UUID uuid) {
        try {
            ResultSet resultSet = Core.getSQLConnection().executeQuery("SELECT * FROM `PlayerData` WHERE UUID=" + uuid.toString() + ";", false);
            boolean contains = resultSet.next();
            resultSet.close();

            return contains;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void savePlayerName(UUID uuid, String playerName) {
        try {
            if (containsPlayer(uuid)) {
                Core.getSQLConnection().executeUpdate("UPDATE `PlayerData` SET PlayerName=" + playerName + " WHERE UUID=" + uuid.toString() + ";");
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveRank(UUID uuid, String rankName) {
        try {
            if (containsPlayer(uuid)) {
                Core.getSQLConnection().executeUpdate("UPDATE `PlayerData` SET Rank=" + rankName + " WHERE UUID=" + uuid.toString() + ";");
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveShards(UUID uuid, int shards) {
        try {
            if (containsPlayer(uuid)) {
                Core.getSQLConnection().executeUpdate("UPDATE `PlayerData` SET Shards=" + shards + " WHERE UUID=" + uuid.toString() + ";");
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveKills(UUID uuid,int overallKills) {
        try {
            if (containsPlayer(uuid)) {
                Core.getSQLConnection().executeUpdate("UPDATE `PlayerData` SET OverallKills=" + overallKills + " WHERE UUID=" + uuid.toString() + ";");
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveDeaths(UUID uuid,int overallDeaths) {
        try {
            if (containsPlayer(uuid)) {
                Core.getSQLConnection().executeUpdate("UPDATE `PlayerData` SET OverallDeaths=" + overallDeaths + " WHERE UUID=" + uuid.toString() + ";");
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveTimePlayed(UUID uuid, int timePlayed) {
        try {
            if (containsPlayer(uuid)) {
                Core.getSQLConnection().executeUpdate("UPDATE `PlayerData` SET TimePlayed=" + timePlayed + " WHERE UUID=" + uuid.toString() + ";");
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* =========================================================
       Adds the player to the DB if they dont exist.
       ========================================================= */
    public static void createPlayerData(UUID uuid) {
        try {
            if (!containsPlayer(uuid)) {
                Core.getSQLConnection().executeUpdate("INSERT INTO `PlayerData` (UUID, PlayerName, Rank, Shards, OverallKills, OverallDeaths, TimePlayed) VALUES('" + uuid.toString() + "', '" + Bukkit.getPlayer(uuid).getName() + "', '" + Rank.ALL.toString() + "', 0,0,0,0);");

                Bukkit.broadcastMessage(ChatColor.DARK_AQUA + Bukkit.getPlayer(uuid).getName() + ChatColor.DARK_PURPLE + " has joined for the first time!");
                Bukkit.getServer().getPlayer(uuid).teleport(new Location(Bukkit.getWorld("world"), 0, 100, 0));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static String getPlayerName(UUID uuid) {
        String value = null;
        try {
            if (containsPlayer(uuid)) {
                ResultSet result = Core.getSQLConnection().executeQuery("SELECT PlayerName FROM `PlayerData` WHERE UUID=" + uuid.toString() + ";", false);
                result.next();
                value = result.getString("PlayerName");
                result.close();
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public synchronized static String getRank(UUID uuid) {
        String value = null;//Rank.ERROR.toString();
        try {
            if (containsPlayer(uuid)) {
                ResultSet result = Core.getSQLConnection().executeQuery("SELECT PlayerName FROM `PlayerData` WHERE UUID=" + uuid.toString() + ";", false);
                result.next();
                value = result.getString("Rank");
                result.close();
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public synchronized static int getShards(UUID uuid) {
        int value = -1;
        try {
            if (containsPlayer(uuid)) {
                ResultSet result = Core.getSQLConnection().executeQuery("SELECT PlayerName FROM `PlayerData` WHERE UUID=" + uuid.toString() + ";", false);
                result.next();
                value = result.getInt("Shards");
                result.close();
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public synchronized static int getOverallKills(UUID uuid) {
        int value = -1;
        try {
            if (containsPlayer(uuid)) {
                ResultSet result = Core.getSQLConnection().executeQuery("SELECT PlayerName FROM `PlayerData` WHERE UUID=" + uuid.toString() + ";", false);
                result.next();
                value = result.getInt("OverallKills");
                result.close();
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public synchronized static int getOverallDeaths(UUID uuid) {
        int value = -1;
        try {
            if (containsPlayer(uuid)) {
                ResultSet result = Core.getSQLConnection().executeQuery("SELECT PlayerName FROM `PlayerData` WHERE UUID=" + uuid.toString() + ";", false);
                result.next();
                value = result.getInt("OverallDeaths");
                result.close();
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public synchronized static int getTimePlayed(UUID uuid) {
        int value = -1;
        try {
            if (containsPlayer(uuid)) {
                ResultSet result = Core.getSQLConnection().executeQuery("SELECT PlayerName FROM `PlayerData` WHERE UUID=" + uuid.toString() + ";", false);
                result.next();
                value = result.getInt("TimePlayed");
                result.close();
            } else {
                createPlayerData(uuid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
}