package net.kranosmc.core.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.kranosmc.core.Core;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Phineas (phineas.pw) on 08/07/2016
 * For Kranos (kranosmc.net)
 */
public class SQLConnection {

    public SQLDatabase MySQL;
    public Connection c;

    public SQLConnection(Plugin plugin, String host, String port, String database, String username, String password) {
        MySQL = new SQLDatabase(plugin, host, port, database, username, password);
    }

    public void openConnection() {
        if(isConnected()) {
            closeConnection();
        }

        try {
            c = MySQL.openConnection();

            executeUpdate("CREATE TABLE IF NOT EXISTS `PlayerData` ( `UUID` VARCHAR(255) NOT NULL , `PlayerName` VARCHAR(255) NOT NULL , `Rank` VARCHAR(255) NOT NULL , `Shards` INT(11) NOT NULL , `OverallKills` INT(11) NOT NULL , `OverallDeaths` INT(11) NOT NULL , `TimePlayed` INT(11) NOT NULL , PRIMARY KEY (`UUID`)) ENGINE = InnoDB;");
            //executeUpdate("CREATE TABLE IF NOT EXISTS `punishments` (uuid VARCHAR(255) NOT NULL, punishment_type VARCHAR(255) NOT NULL, severity VARCHAR(255) NOT NULL, time_issued VARCHAR(255) NOT NULL, time_end VARCHAR(255) NOT NULL, issued_by VARCHAR(255) NOT NULL, reason VARCHAR(255) NOT NULL);");

            new BukkitRunnable() {
                public void run() {
                    openConnection();
                }
            }.runTaskLater(Core.getInstance(), 100L);
        } catch (ClassNotFoundException e) {e.printStackTrace();
        } catch (SQLException e) {e.printStackTrace(); }
    }

    public boolean isConnected() {
        try {
            if(c.isClosed()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void closeConnection() {
        if(isConnected()) {
            try {
                c.close();
            } catch (SQLException e) {}
        }
    }

    public ResultSet executeQuery(String statement, boolean next) {
        if(isConnected()) {
            try {
                Statement s = c.createStatement();
                ResultSet res = s.executeQuery(statement);
                if(next) {res.next(); }
                return res;
            } catch (SQLException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean executeUpdate(String statement) {
        if(isConnected()) {
            try {
                Statement s = c.createStatement();
                s.executeUpdate(statement);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
}
